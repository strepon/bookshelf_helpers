A script and resources to convert Czech LibreOffice books from ODF to HTML format published in the [LibreOffice Bookshelf](https://books.libreoffice.org).

## How to add a book

1. Apply the `LO_UG_Chapters_2Xx_EN.ott` template (from the [SanityCheck extension](https://github.com/Bantoniof/LibreOffice-SanityCheck/tree/master/SanityCheck/template/en-US/LO_Doc)) to the source ODT files (by using the Template Changer extension).
1. Convert the modified ODT files to HTML by Writer2Latex tool:
    ```
    ./w2l -xhtml -save_images_in_subdir=true ../odt/GS73 ../cs/GS73
    ```
1. Process the resulting HTML files by the script:
    ```
    ./postprocess.py ../bookshelf/cs/GS73
    ```
1. Add front page thumbnails to the `images` folder.

1. Add the book and related links to the home page `index.html`.