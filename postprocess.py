#!/usr/bin/env python3

import os
import argparse
import shutil
import re
from bs4 import BeautifulSoup

def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

parser = argparse.ArgumentParser("postprocess")
parser.add_argument("directory", help="Directory containing book chapters in HTML.", type=dir_path)
workdir = parser.parse_args().directory

book_code = os.path.basename(os.path.normpath(workdir))
book_title = None

with open("./addtohead.txt") as opened_file:
    head_addition = opened_file.read()
head_addition = head_addition.replace("GS72.js", f"{book_code}.js")

index_file = os.path.join(workdir, "index.html")
if os.path.isfile(index_file):
    print("Removing index.html")
    os.remove(index_file)

chapters = []
for html_file in os.listdir(workdir):
    html_file_path = os.path.join(workdir, html_file)
    if os.path.isfile(html_file_path) and os.path.splitext(html_file_path)[1] == ".html" and html_file != f"{book_code}.html":
        with open(html_file_path) as opened_file:
            html_content = opened_file.read()

        html = BeautifulSoup(html_content, "html.parser")
        chapters.append({ "file": html_file, "title": html.title.text.strip() })

        # old format before guides for 7.2
        if html.find(id="SECGUIDEregion") == None:
            logo_region = html.new_tag("div", id="SECLOGOregion")
            logo_region.append(html.find("body").find("p").extract())
            guide_region = html.new_tag("div", id="SECGUIDEregion")
            guide_region.append(html.find("body").find("p").extract())
            title_region = html.new_tag("div", id="SECTITLEregion")
            for p in range(0, 2):
                title_region.append(html.find("body").find("p").extract())
            display_area_region = html.new_tag("div", id="SECDISPLAYAREAregion")
            body_elements = html.find("body").find_all(recursive=False)
            for element in body_elements:
                display_area_region.append(element.extract())

            html.find("body").insert(0, display_area_region)
            html.find("body").insert(0, title_region)
            html.find("body").insert(0, guide_region)
            html.find("body").insert(0, logo_region)

        if book_title == None:
            book_title = html.find(id="SECGUIDEregion").find("p").text.strip()

        if len(html.head.find_all("link")) < 2:
            print("Adding head content")
            html.head.append(BeautifulSoup(head_addition, "html.parser"))

        styles = html.head.style.string.split("\n")
        if len(styles) > 1 and styles[1].strip().startswith("body"):
            print("Removing body style")
            styles.pop(0)
            styles.pop(0)
            html.head.style.string = "\n".join(styles)

        body_divs = html.body.find_all("div")
        if len(body_divs) > 0 and (not "id" in body_divs[-1].attrs or body_divs[-1].attrs["id"] != "SECDONATIONregion"):
            print("Adding chapter sections")
            section_ids = ["SECTOCregion", "SECBOOKTOCregion", "SECSEARCHregion", "SECIMPRINTregion", "SECNAVregion", "SECDONATIONregion"]
            for section_id in section_ids:
                html.body.append(BeautifulSoup(f'<div id="{section_id}"></div>', "html.parser"))

        chapter_toc_region = html.find(id="SECTOCregion")
        # inconsistent IDs as "tocGS2410" or "tocGS2480"
        book_code_without_version = re.sub(r"\d", "", book_code)
        toc_ids = ["Sumario1", "TableofContents1", "TableofContents2", re.compile(rf"toc{book_code_without_version}.*")]
        chapter_toc = None
        for toc_id in toc_ids:
            chapter_toc = html.find(id=toc_id)
            if chapter_toc != None:
                break
        if chapter_toc_region.find(id=toc_id) == None:
            print("Moving chapter table of contents")
            chapter_toc.extract()
            chapter_toc_region.append(chapter_toc)
        footnotes = html.find_all("hr", attrs={"class":"footnoterule"})
        if len(footnotes) > 0:
            footnotes_area = footnotes[0].parent.extract()
            html.find(id="SECDISPLAYAREAregion").append(footnotes_area)

        with open(html_file_path, "w") as opened_file:
            output = str(html)
            output = output.replace('<p class="Textbody" lang="cs-CZ" xml:lang="cs-CZ"> </p>', '')
            opened_file.write(output)
            print(f"Output written to {html_file_path}")

def get_chapter_number(chapter):
    chapter_without_book = chapter["file"].replace(book_code, "")
    return int(chapter_without_book[0:2])

chapters.sort(key=get_chapter_number)

book_toc = []
for chapter in chapters:
    book_toc.append(f"{{ file:'{chapter['file']}', title: '{chapter['title']}' }}")
book_toc = "[\n" + ",\n".join(book_toc) + "\n]"

print("Creating JS file for book")
with open("book.js") as js_file:
    js_template = js_file.read()

js_template = js_template.replace("var ChapterMap=[]", f"var ChapterMap={book_toc}");
js_template = js_template.replace("$BOOK_CODE", book_code);

js_file_path = os.path.join(workdir, f"{book_code}.js")
with open(js_file_path, "w") as js_file:
    js_file.write(js_template)

print("Creating HTML file for book")
with open("book.html") as html_file:
    html_template = BeautifulSoup(html_file.read(), "html.parser")

chapter_list = html_template.find(id="SECDISPLAYAREAregion").find("ul")
for chapter in chapters:
    chapter_list.append(BeautifulSoup(f"<li><p><a href=\"{chapter['file']}\">{chapter['title']}</a></p></li>", "html.parser"))

html_template.find(id="SECDISPLAYAREAregion").find("img")["src"] = f"../images/thumbs/{book_code}.png"
scripts = html_template.find_all("script")
scripts[len(scripts) - 1]["src"] = f"{book_code}.js"
html_template.find(id="SECGUIDEregion").find("p").string = book_title
html_template.title.string = book_title

html_file_path = os.path.join(workdir, f"{book_code}.html")
with open(html_file_path, "w") as html_file:
    html_file.write(str(html_template))

print("Copying resource files")
for resource_file in ["LibreOfficeLogo.png", "guidecolors.css"]:
    shutil.copyfile(os.path.join("resources", resource_file), os.path.join(workdir, resource_file))
