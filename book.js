/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
var ChapterMap=[]

var book_toc = document.getElementById('SECBOOKTOCregion');
if (book_toc != null){
    var a = '<p style="text-align:center;"><img src="../images/$BOOK_CODE.png"/></p><ul>';
    for (i in ChapterMap){
        a +='<li><p><a href="'+ ChapterMap[i].file + '">' + ChapterMap[i].title + '</a></p></li>';
    }
    a +='</ul>';
    book_toc.innerHTML=a;
}

var d_search = document.getElementById('SECSEARCHregion');
var s= '<form name="P" method="get" action="/cs/$BOOK_CODE/search" target="_top">';
s = s+ '<input id="omega-autofocus" type="search" name="P"/><input type="submit" class="xapian-omega-search-button" value="&#x1f50d;"/>';
s = s+ '</form>';
d_search.innerHTML =s;

var _logo = document.getElementById('SECLOGOregion');
_logo.innerHTML = '<a href="../index.html"><img src="LibreOfficeLogo.png" alt="LibreOfficeLogo"/></a>';

var g =  document.getElementById('SECDONATIONregion');
g.innerHTML = '<div class="donation" style="background-color: #106802; font-size: 1.2rem; padding: 10px 20px 10px 20px; border-radius: 10px"><a href="https://cs.libreoffice.org/donate/?pk_campaign=help" target="_blank" style="color: #ccf4c6; text-decoration:none">Podpořte LibreOffice!</a></div>'

var im = document.getElementById('SECIMPRINTregion');
im.innerHTML = '<p itemscope="true" itemtype="http://schema.org/Organization"><meta itemprop="name" content="The Document Foundation"><meta itemprop="legalName" content="The Document Foundation"><meta itemprop="alternateName" content="TDF"><meta itemprop="publishingPrinciples" content="https://www.libreoffice.org/imprint"><a href="https://www.libreoffice.org/imprint" target="_blank">Impressum (Legal Info)</a> | <a href="https://www.libreoffice.org/privacy" target="_blank">Privacy Policy</a> | <a href="https://www.documentfoundation.org/statutes.pdf" target="_blank">Statutes (non-binding English translation)</a> - <a href="https://www.documentfoundation.org/satzung.pdf" target="_blank">Satzung (binding German version)</a> | Copyright information: Unless otherwise specified, all text and images on this website are licensed under the <a href="https://www.libreoffice.org/download/license/" target="_blank">Mozilla Public License v2.0</a>. “LibreOffice” and “The Document Foundation” are registered trademarks of their corresponding registered owners or are in actual use as trademarks in one or more countries. Their respective logos and icons are also subject to international copyright laws. Use thereof is explained in our <a href="https://wiki.documentfoundation.org/TradeMark_Policy" target="_blank">trademark policy</a>. LibreOffice was based on OpenOffice.org.</p>'

